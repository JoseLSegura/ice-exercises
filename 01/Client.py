#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('Printer.ice')
import Example


class Client(Ice.Application):
    def run(self, argv):
        if len(argv) < 3:
            print('Error, not enough arguments')
            return 1

        proxy = self.communicator().stringToProxy(argv[1])
        printer = Example.PrinterPrx.checkedCast(proxy)

        if not printer:
            print('Invalid proxy')
            return 2

        printer.write(argv[2])
        return 0


sys.exit(Client().main(sys.argv))
