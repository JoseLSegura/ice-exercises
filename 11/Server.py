#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys
import time

import Ice
Ice.loadSlice('Printer.ice')
import Example


class PrinterI(Example.Printer):
    def __init__(self, prefix):
        self.n = 0
        self.prefix = prefix

    def write(self, message, current):
        time.sleep(3)
        print("{2} {0}: {1}".format(self.n, message, self.prefix))
        sys.stdout.flush()
        self.n += 1


class Server(Ice.Application):
    def run(self, argv):
        broker = self.communicator()
        servant = PrinterI('printer1')

        adapter = broker.createObjectAdapter("PrinterAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity("printer1"))

        print(proxy)
        sys.stdout.flush()

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


server = Server()
sys.exit(server.main(sys.argv))
