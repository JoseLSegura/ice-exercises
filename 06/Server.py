#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('Printer.ice')
import Example


class PrinterI(Example.Printer):
    def __init__(self, prefix):
        self.n = 0
        self.prefix = prefix

    def write(self, message, current):
        print("{2} {0}: {1}".format(self.n, message, self.prefix))
        sys.stdout.flush()
        self.n += 1


class Server(Ice.Application):
    def run(self, argv):
        broker = self.communicator()

        identity1 = 'printer1'
        identity2 = 'printer2'
        servant = PrinterI(identity1)
        servant = PrinterI(identity2)

        adapter = broker.createObjectAdapter("PrinterAdapter")
        proxy1 = adapter.add(servant, broker.stringToIdentity(identity1))
        proxy2 = adapter.add(servant, broker.stringToIdentity(identity2))

        print(proxy1)
        print(proxy2)
        sys.stdout.flush()

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


server = Server()
sys.exit(server.main(sys.argv))
