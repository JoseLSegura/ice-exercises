#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('Printer.ice')
import Example


class Client(Ice.Application):
    def run(self, argv):
        if len(argv) < 3:
            print('Error, not enough arguments')
            return 1

        proxy = self.communicator().stringToProxy(argv[1])
        
        printer1 = Example.PrinterPrx.checkedCast(proxy)
        printer2 = Example.PrinterPrx.checkedCast(proxy)

        if not printer1 or not printer2:
            print('Invalid proxy')
            return 2

        printer1.write(argv[2])
        printer2.write(argv[2])
        return 0


sys.exit(Client().main(sys.argv))
