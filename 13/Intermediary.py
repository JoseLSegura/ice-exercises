#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('Printer.ice')

import Example


class PrinterI(Example.Printer):
    def __init__(self, printer):
        self.printer = printer

    def write(self, message, current):
        print("Received: {}".format(message))
        sys.stdout.flush()
        self.printer.write("redirect: {}".format(message))


class Server(Ice.Application):
    def run(self, argv):
        broker = self.communicator()
        proxy = broker.propertyToProxy("Printer")
        identity = broker.getProperties().getPropertyWithDefault(
            "Identity", "intermediary1")

        printer = Example.PrinterPrx.checkedCast(proxy)
        servant = PrinterI(printer)

        adapter = broker.createObjectAdapter("PrinterAdapter")
        proxy = adapter.add(servant, broker.stringToIdentity(identity))

        print(proxy)
        sys.stdout.flush()

        adapter.activate()
        self.shutdownOnInterrupt()
        broker.waitForShutdown()

        return 0


server = Server()
sys.exit(server.main(sys.argv))
